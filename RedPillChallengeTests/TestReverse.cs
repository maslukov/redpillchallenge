﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RedPillChallengerService.Logic;


namespace RedPillChallengeTests
{
    [TestClass]
    public class TestReverse
    {
        [TestMethod]
        public void EmptyText()
        {
            var reversed = WordReverser.ReverseWords("");
            Assert.AreEqual(reversed, "");
        }

        [TestMethod]
        public void SingleSpace()
        {
            var reversed = WordReverser.ReverseWords(" ");
            Assert.AreEqual(reversed, " ");
        }


        [TestMethod]
        public void SingleWordCaseShouldBePreserved()
        {
            var reversed = WordReverser.ReverseWords("Alex");
            Assert.AreEqual(reversed, "xelA");
        }

        [TestMethod]
        public void SpaceIsSeparator()
        {
            var reversed = WordReverser.ReverseWords("Red Pill");
            Assert.AreEqual(reversed, "deR lliP");
        }


        [TestMethod]
        public void CarriageReturnIsSeparator()
        {
            var reversed = WordReverser.ReverseWords("good\rdog");
            Assert.AreEqual("doog\rgod", reversed);
        }

        [TestMethod]
        public void EndOfLineIsSeparator()
        {
            var reversed = WordReverser.ReverseWords("good\ndog");
            Assert.AreEqual("doog\ngod", reversed);
        }

        [TestMethod]
        public void DoubleSpacesShouldBePreserved()
        {
            var reversed = WordReverser.ReverseWords("Red  Pill");
            Assert.AreEqual(reversed, "deR  lliP");
        }

        [TestMethod]
        public void LeadingSpacesShouldBePreserved()
        {
            var reversed = WordReverser.ReverseWords("  Red  Pill");
            Assert.AreEqual(reversed, "  deR  lliP");
        }

        [TestMethod]
        public void TrailingSpacesShouldBePreserved()
        {
            var reversed = WordReverser.ReverseWords("Red  Pill  ");
            Assert.AreEqual(reversed, "deR  lliP  ");
        }


        [TestMethod]
        public void MultipleWords()
        {
            var reversed = WordReverser.ReverseWords("Red Pill Challenge");
            Assert.AreEqual(reversed, "deR lliP egnellahC");
        }

        [TestMethod]
        public void CommasAndPunctuationAreNotSeparators()
        {
            var reversed = WordReverser.ReverseWords("one,two.three");
            Assert.AreEqual(reversed, "eerht.owt,eno");
        }

   



    }
}