﻿
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RedPillChallengeService;
using RedPillChallengerService.Logic;

namespace RedPillChallengeTests
{
    [TestClass]
    public class TestTriangleDetector
    {
        void VerifyTriangleType(ETriangleType expectedTrangleType, int a, int b, int c)
        {
            // Verify all possible combination of passed side length
            MustMatch(expectedTrangleType, a, b, c);
            MustMatch(expectedTrangleType, a, c, b);
            MustMatch(expectedTrangleType, b, a, c);
            MustMatch(expectedTrangleType, b, c, a);
            MustMatch(expectedTrangleType, c, a, b);
            MustMatch(expectedTrangleType, c, b, a);        
        }

        private static void MustMatch(ETriangleType expectedTriangleType, int a, int b, int c)
        {
            Assert.AreEqual(expectedTriangleType, TriangleDetector.DetectTriangleType(a, b, c),
                string.Format("Passing a:{0} b:{1} c:{2}", a, b, c));
        }

        [TestMethod]
        public void Equaterial()
        {                   
            VerifyTriangleType(ETriangleType.Equilateral, 1,1,1);
        }

        [TestMethod]
        public void Equaterial_MaxIntegerValues()
        {
            VerifyTriangleType(ETriangleType.Equilateral, Int32.MaxValue, Int32.MaxValue, Int32.MaxValue);
        }

        [TestMethod]
        public void Isosceles()
        {
 
            VerifyTriangleType(ETriangleType.Isosceles, 2,2,1);
        }

        [TestMethod]
        public void Incorrect_Isosceles()
        {      
            VerifyTriangleType(ETriangleType.Error, 10,1,1);
        }

        [TestMethod]
        public void Incorrect_Isosceles_SideSum()
        {
            VerifyTriangleType(ETriangleType.Error, 2, 1, 1);
        }

        [TestMethod]
        public void Incorrect_Scalene_SideSum()
        {
            VerifyTriangleType(ETriangleType.Error, 1, 2, 3);
        }

        [TestMethod]
        public void Incorrect_MinIntegerValues()
        {
            VerifyTriangleType(ETriangleType.Error, Int32.MinValue, Int32.MinValue, Int32.MinValue);
        }

        [TestMethod]
        public void Scalene()
        {
            VerifyTriangleType(ETriangleType.Scalene, 2, 3, 4);
        }

        [TestMethod]
        public void Incorrect_Scalene()
        {
            VerifyTriangleType(ETriangleType.Error, 2, 3, 6);
        }

        [TestMethod]
        public void Zero_Isosceles()
        {
            VerifyTriangleType(ETriangleType.Error, 0, 1, 1);
        }

        [TestMethod]
        public void Zero_Equaterial()
        {
            VerifyTriangleType(ETriangleType.Error, 0, 0, 0);
        }

        [TestMethod]
        public void NegativeLength_Equaterial()
        {
            VerifyTriangleType(ETriangleType.Error, -1, -1, -1);
        }

        [TestMethod]
        public void NegativeLength_Isosceles()
        {
            VerifyTriangleType(ETriangleType.Error, 1, -2, -2);
        }

    }
}
