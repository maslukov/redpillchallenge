﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RedPillChallengerService.Logic;

namespace RedPillChallengeTests
{
    [TestClass]
    public class TestFibonacciCalculator
    {
        [TestMethod]
        public void PositiveSimpleSequence()
        {
            Assert.AreEqual(0, FibonacciCalculator.GetFibNumber(0));
            Assert.AreEqual(1, FibonacciCalculator.GetFibNumber(1));
            Assert.AreEqual(1, FibonacciCalculator.GetFibNumber(2));
            Assert.AreEqual(2, FibonacciCalculator.GetFibNumber(3));
            Assert.AreEqual(3, FibonacciCalculator.GetFibNumber(4));
            Assert.AreEqual(5, FibonacciCalculator.GetFibNumber(5));
        }

        [TestMethod]
        public void NegativeSimpleSequence()
        {
            Assert.AreEqual(0, FibonacciCalculator.GetFibNumber(0));
            Assert.AreEqual(1, FibonacciCalculator.GetFibNumber(-1));
            Assert.AreEqual(-1, FibonacciCalculator.GetFibNumber(-2));
            Assert.AreEqual(2, FibonacciCalculator.GetFibNumber(-3));
            Assert.AreEqual(-3, FibonacciCalculator.GetFibNumber(-4));
            Assert.AreEqual(5, FibonacciCalculator.GetFibNumber(-5));
        }


        [TestMethod]
        public void Fifty()
        {
            Assert.AreEqual(12586269025, FibonacciCalculator.GetFibNumber(50));
            Assert.AreEqual(-12586269025, FibonacciCalculator.GetFibNumber(-50));
        }

        [TestMethod]
        public void MaximumAllowed()
        {
            Assert.AreEqual(7540113804746346429, FibonacciCalculator.GetFibNumber(92));
            Assert.AreEqual(-7540113804746346429, FibonacciCalculator.GetFibNumber(-92));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void OutOfRangeException()
        {
            FibonacciCalculator.GetFibNumber(93);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MinimumLongNumber()
        {
            FibonacciCalculator.GetFibNumber(long.MinValue);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void MaximumLongNumber()
        {
            FibonacciCalculator.GetFibNumber(long.MaxValue);
        }

    }
}
