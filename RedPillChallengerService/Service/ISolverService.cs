﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace RedPillChallengeService
{

    [DataContract(Name = "TriangleType", Namespace = ServiceNamespaces.TargetNamespace)]
    public enum ETriangleType
    {
        [EnumMember]
        Error = 0,

        [EnumMember]
        Equilateral = 1,

        [EnumMember]
        Isosceles = 2,

        [EnumMember]
        Scalene = 3,
    }

    [ServiceContract(Namespace = ServiceNamespaces.TargetNamespace)]
    public interface ISolverService
    {
        [OperationContract(Action = ServiceNamespaces.WhatIsYourTokenAction)]
        Guid WhatIsYourToken();

        [OperationContract(Action = ServiceNamespaces.FibonacciAction)]
        [FaultContract(typeof(ArgumentOutOfRangeException))]
        long FibonacciNumber(long n);

        [OperationContract(Action = ServiceNamespaces.WhatShapeIsIt)]
        ETriangleType WhatShapeIsThis(int a, int b, int c);

        [OperationContract(Action = ServiceNamespaces.ReverseWordsAction)]
        [FaultContract(typeof(ArgumentNullException))]
        string ReverseWords(string s);
    }
}