﻿namespace RedPillChallengeService
{
    public class ServiceNamespaces
    {
        public const string TargetNamespace = "http://KnockKnock.readify.net";

        public const string WhatIsYourTokenAction   = "http://KnockKnock.readify.net/IRedPill/WhatIsYourToken";
        public const string FibonacciAction         = "http://KnockKnock.readify.net/IRedPill/FibonacciNumber";
        public const string WhatShapeIsIt           = "http://KnockKnock.readify.net/IRedPill/WhatShapeIsThis";
        public const string ReverseWordsAction      = "http://KnockKnock.readify.net/IRedPill/ReverseWords";

    }
}
