﻿using System;
using System.Diagnostics;
using System.ServiceModel;
using RedPillChallengerService.Logic;


namespace RedPillChallengeService
{
    [ServiceBehavior(Namespace = ServiceNamespaces.TargetNamespace)]
    public class SolverService : ISolverService
    {
        // magic number - unique identified of this service.
        private static readonly Guid _token = Guid.Parse("71e362a9-ee74-4ddd-bef9-287617571424");

        public Guid WhatIsYourToken()
        {
            return _token;
        }

        public long FibonacciNumber(long n)
        {
            Trace.TraceInformation("FibonacciNumber: {0} ",n);
            return InvokeAction(() => FibonacciCalculator.GetFibNumber(n));
        }

        public ETriangleType WhatShapeIsThis(int a, int b, int c)
        {
            Trace.TraceInformation("WhatShapeIsThis: {0} {1} {2} " ,a,b,c);
            return InvokeAction(() => TriangleDetector.DetectTriangleType(a,b,c));
        }

        public string ReverseWords(string s)
        {
            Trace.TraceInformation("ReverseWords: {0}", s);
            return InvokeAction(() => WordReverser.ReverseWords(s));
        }

        /// <summary>
        /// Execute code and log any exceptions that occur
        /// </summary>
        /// <typeparam name="T">the type of return argument</typeparam>
        /// <param name="action">code to execute (as a delegate)</param>
        /// <returns>retuns back result from executable action</returns>
        private T InvokeAction<T>(Func<T> action )
        {
            try
            {
                return action();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Trace.TraceInformation("Expected Error happened:" + ex);
                throw new FaultException<ArgumentOutOfRangeException>(ex,ex.Message);
            }
            catch (ArgumentNullException ex)
            {
                Trace.TraceInformation("Expected Error happened:" + ex);
                throw new FaultException<ArgumentNullException>(ex,ex.Message);
            }
            catch(Exception ex)
            {
                Trace.TraceError("Error happened:" + ex);
                throw new FaultException<Exception>(ex,ex.Message);
            }
           
        }
    }
}
