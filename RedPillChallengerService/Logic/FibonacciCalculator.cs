﻿using System;

namespace RedPillChallengerService.Logic
{
    public class FibonacciCalculator
    {
        /// <summary>
        /// Calculate and return Nth Fibonacci number. Support both positive and negative fibonacci sequence.
        /// Uses straightforward sum algorithm
        /// </summary>
        /// <param name="nth">Fibonacci number sequence number</param>
        /// <returns>Fibonacci number value</returns>
        public static long GetFibNumber(long nth)
        {
            // check for the boundaries first, before taking the absolute value         
            if (nth > 92 || nth<-92)
                throw new ArgumentOutOfRangeException("nth","Fib(>92) will cause a 64-bit integer overflow.");

            var absNth = Math.Abs(nth);

            // return predefined fibonacci number for 0,1,-1.
            if (absNth <= 1)
                return absNth;

            // Calculate using straightforward approach 
            // For positive: Fn = Fn-2+Fn-1, for negative: Fn = Fn-2 - Fn-1.
            // If speed is concern use fast fibonacci calculation (Fast doubling algorithm).

            long Fn = 0;        // fibonacci number at step Nth

            long Fn_1 = 1;      // fibonacci number at step Nth-1
            long Fn_2 = 0;      // fibonacci number at step Nth-2

            for (int i = 2; i <= absNth; i++)
            {
                if(nth<0)
                    Fn = -Fn_1 + Fn_2;   
                else
                    Fn = Fn_1 + Fn_2;   
              
                Fn_2 = Fn_1;
                Fn_1 = Fn;
            }

            return Fn;
        }
    }
}
