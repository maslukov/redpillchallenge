﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedPillChallengerService.Logic
{
    /// <summary>
    /// Reverse words in a supplied text. Use predefined separators to separate the words
    /// </summary>
    public class WordReverser
    {
        private static readonly char[] Separators = {' ','\r','\n'};

        /// <summary>
        /// Reverse words in a supplied text. The words are split using the following separators ' ','\r','\n'
        /// </summary>
        /// <param name="text">input text </param>
        /// <returns>result text where all words are reversed.</returns>
        public static string ReverseWords(string text)
        {
            if (text == null)
                throw new ArgumentNullException("text");

            var reversedText = new StringBuilder(text.Length);
            
            foreach (var word in ParseWords(text,Separators))
            {
                reversedText.Append(Reverse(word));

                AppendSeparatorFromOriginalText(text, reversedText);
            }

            return reversedText.ToString();
        }


        /// <summary>
        /// Reverse a single word
        /// </summary>
        /// <param name="word">input word to be reversed</param>
        /// <returns>reversed word</returns>
        private static string Reverse(string word)
        {
            var reversed = new char[word.Length];
            for (int i = 0; i < word.Length; i++)
            {
                reversed[i] = word[word.Length - i - 1];
            }
            return new string(reversed);
        }

        /// <summary>
        /// Parse input text into array of words using predefined separator list
        /// </summary>
        /// <param name="text">input text to be parsed</param>
        /// <param name="separators">list of separators</param>
        /// <returns>array of words inside the text</returns>
        private static IEnumerable<string> ParseWords(string text,char[] separators)
        {
            return text.Split(separators);
        }

        /// <summary>
        ///  take separator from original text and add at the end of the reversed Text
        /// </summary>
        /// <param name="text">original text</param>
        /// <param name="reversedText">reversed text - the separator will be appended to it</param>
        private static void AppendSeparatorFromOriginalText(string text, StringBuilder reversedText)
        {
            if (reversedText.Length < text.Length)
                reversedText.Append(text[reversedText.Length]);
        }
    }
}