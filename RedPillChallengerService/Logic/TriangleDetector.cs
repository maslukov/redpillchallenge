﻿using RedPillChallengeService;

namespace RedPillChallengerService.Logic
{

    public class TriangleDetector
    {
        /// <summary>
        /// Identify the type of triangle by analyzing triangle sides sizes
        /// </summary>
        /// <param name="a">Side A length</param>
        /// <param name="b">Side B length</param>
        /// <param name="c">Size C length</param>
        /// <returns>Triangle type - Equilateral , Isosceles or Scalene. Error will be returned if side sizes are not valid (negative or triangle can not be built using these sides)</returns>
        public static ETriangleType DetectTriangleType(int a, int b, int c)
        {
            if (!AllSidesAreValid(a, b, c))
                return ETriangleType.Error;

            if (a == b)
            {
                if (b == c)
                    return ETriangleType.Equilateral;
 
                return ETriangleType.Isosceles;
            }

            if (a == c)
            {
                if (b == c)
                    return ETriangleType.Equilateral;
            
                return ETriangleType.Isosceles;
            }

            if (b == c)
            {
                if (a == b)
                    return ETriangleType.Equilateral;           

                return ETriangleType.Isosceles;
            }
            
            return ETriangleType.Scalene;
            
        }

        private static bool AllSidesAreValid(int a, int b, int c)
        {
            if (a <= 0 || b <= 0 || c <= 0)
                return false;

            // avoid integer sum overflow by casting to long
            if ((long)a + b <= c ||
                 (long)a + c <= b ||
                 (long)b + c <= a)
                return false;

            return true;
        }

    }
}
